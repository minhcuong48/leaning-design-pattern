package com.cuong.learn_design_pattern.structural.flyweight;

import java.util.HashMap;

public class PlayerFlyweightFactory {
    private static HashMap<PlayerType, Player> players = new HashMap<>();

    public static Player getPlayer(PlayerType playerType) {
        Player player = null;
        if (players.containsKey(playerType)) {
            player = players.get(playerType);
        } else {
            switch (playerType) {
                case Terrorist:
                    player = new Terrorist(Misson.PlantABomb);
                    break;
                case CounterTerrorist:
                    player = new CounterTerrorist(Misson.DiffuseABomb);
                    break;
            }
        }
        players.put(playerType, player);
        return player;
    }
}
