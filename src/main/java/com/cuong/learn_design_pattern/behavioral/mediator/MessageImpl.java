package com.cuong.learn_design_pattern.behavioral.mediator;

public class MessageImpl extends Message {
    private String message;
    private User sender;
    private User receiver;

    public MessageImpl(String message, User sender, User receiver) {
        this.message = message;
        this.sender = sender;
        this.receiver = receiver;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public User getSender() {
        return sender;
    }

    @Override
    public User getReceiver() {
        return receiver;
    }
}
