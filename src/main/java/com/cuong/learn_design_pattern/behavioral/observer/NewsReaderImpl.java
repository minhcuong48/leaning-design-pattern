package com.cuong.learn_design_pattern.behavioral.observer;

public class NewsReaderImpl extends NewsReader {
    private String name;

    public NewsReaderImpl(String name) {
        this.name = name;
    }

    @Override
    public void getNews(String news) {
        System.out.println(name + " got a news: " + news);
    }
}
