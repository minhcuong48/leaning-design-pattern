package com.cuong.learn_design_pattern.behavioral.mediator;

public class Client {
    public static void main(String[] args) {
        ChatSystemMediator chatSystemMediator = new ChatSystemMediatorImpl();
        User user1 = new UserImpl(chatSystemMediator, "User1");
        User user2 = new UserImpl(chatSystemMediator, "User2");

        user1.sendMessage("This message from user1 to user2", user2);
    }
}
