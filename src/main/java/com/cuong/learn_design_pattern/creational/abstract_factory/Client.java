package com.cuong.learn_design_pattern.creational.abstract_factory;

import com.cuong.learn_design_pattern.creational.abstract_factory.factories.AppleFactory;
import com.cuong.learn_design_pattern.creational.abstract_factory.factories.DeviceFactory;
import com.cuong.learn_design_pattern.creational.abstract_factory.factories.SamsungFactory;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Laptop;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Phone;

public class Client {

    private enum Factory {
        Apple, Samsung
    }

    public static void main(String[] args) {
        DeviceFactory factory = getFactory(Factory.Apple);
        Phone phone = factory.createPhone();
        Laptop laptop = factory.createLaptop();
        System.out.println("Phone name: " + phone.getDeviceName());
        System.out.println("Laptop name: " + laptop.getDeviceName());
    }

    public static DeviceFactory getFactory(Factory factory) {
        switch (factory) {
            case Apple: return new AppleFactory();
            case Samsung: return new SamsungFactory();
        }
        return null;
    }
}
