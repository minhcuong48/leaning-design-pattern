package com.cuong.learn_design_pattern.behavioral.iterator;

public class StudentIteratorImpl extends StudentIterator {
    private Class clazz;
    private int currentIndex = -1;

    public StudentIteratorImpl(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public Student getNext() {
        currentIndex += 1;
        if (currentIndex >= clazz.getStudents().size()) {
            return null;
        } else {
            return clazz.getStudents().get(currentIndex);
        }
    }
}
