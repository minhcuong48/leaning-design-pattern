package com.cuong.learn_design_pattern.behavioral.visitor.elements;

import com.cuong.learn_design_pattern.behavioral.visitor.visitors.ShowVisitor;

public class Ruler extends ShowVisitable {
    private int length;

    public Ruler(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public void accept(ShowVisitor showVisitor) {
        showVisitor.visit(this);
    }
}
