package com.cuong.learn_design_pattern.structural.proxy;

public class Client {
    public static void main(String[] args) {
        User user = new UserProxy("GMC");
        user.showName();
    }
}
