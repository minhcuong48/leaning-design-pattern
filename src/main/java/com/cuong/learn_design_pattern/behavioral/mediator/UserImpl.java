package com.cuong.learn_design_pattern.behavioral.mediator;

public class UserImpl extends User {
    private ChatSystemMediator chatSystemMediator;
    private String name;

    public UserImpl(ChatSystemMediator chatSystemMediator, String name) {
        this.chatSystemMediator = chatSystemMediator;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message, User user) {
        Message mess = new MessageImpl(message, this, user);
        chatSystemMediator.sendMessage(mess);
    }

    @Override
    public void getMessage(Message message) {
        System.out.println("from " + message.getSender().getName() + " to " + message.getReceiver().getName() + " : " + message.getMessage());
    }
}
