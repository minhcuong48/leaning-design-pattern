package com.cuong.learn_design_pattern.behavioral.command;

public class ReportCommandImpl extends ReportCommand {
    private Employee employee;

    public ReportCommandImpl(Employee employee) {
        this.employee = employee;
    }

    @Override
    public void report(Boss toBoss) {
        employee.report(toBoss);
    }
}
