package com.cuong.learn_design_pattern.behavioral.iterator;

public abstract class StudentIterable {
    public abstract StudentIterator getIterator();
}
