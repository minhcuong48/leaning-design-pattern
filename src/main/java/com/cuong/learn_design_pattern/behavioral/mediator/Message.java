package com.cuong.learn_design_pattern.behavioral.mediator;

public abstract class Message {
    public abstract String getMessage();
    public abstract User getSender();
    public abstract User getReceiver();
}
