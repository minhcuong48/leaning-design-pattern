package com.cuong.learn_design_pattern.structural.bridge.abstraction;

import com.cuong.learn_design_pattern.structural.bridge.implementor.CpuCoreI3;
import com.cuong.learn_design_pattern.structural.bridge.implementor.RamDDR3;

public class SurfaceBook extends Computer {
    public SurfaceBook() {
        this.cpu = new CpuCoreI3();
        this.ram = new RamDDR3();
    }

    @Override
    public void run() {
        System.out.println("Surface Book");
        this.cpu.run();
        this.ram.run();
    }
}
