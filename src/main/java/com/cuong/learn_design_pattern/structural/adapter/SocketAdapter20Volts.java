package com.cuong.learn_design_pattern.structural.adapter;

public interface SocketAdapter20Volts {
    Volts get20Volts();
}
