package com.cuong.learn_design_pattern.behavioral.strategy;

public class Software {
    private Algorithm algorithm;

    public Software(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public void run() {
        algorithm.runAlgorithm();
    }
}
