package com.cuong.learn_design_pattern.creational.factory_method;

public class WordDocument extends Document {
    @Override
    public String getType() {
        return "Word Document";
    }
}
