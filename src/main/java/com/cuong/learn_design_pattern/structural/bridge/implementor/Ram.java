package com.cuong.learn_design_pattern.structural.bridge.implementor;

public abstract class Ram {
    public abstract void run();
}
