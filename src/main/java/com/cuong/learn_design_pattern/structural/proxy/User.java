package com.cuong.learn_design_pattern.structural.proxy;

public abstract class User {
    public abstract void showName();
}
