package com.cuong.learn_design_pattern.structural.flyweight;

public enum Weapon {
    AK, M4A1
}
