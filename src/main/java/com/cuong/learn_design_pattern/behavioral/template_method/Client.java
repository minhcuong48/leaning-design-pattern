package com.cuong.learn_design_pattern.behavioral.template_method;

public class Client {
    public static void main(String[] args) {
        Algorithm algorithm = new ConcreteAlgorithm();
        algorithm.runAlgorithm();
    }
}
