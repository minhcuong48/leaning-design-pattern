package com.cuong.learn_design_pattern.behavioral.chain_of_responsibility;

public abstract class Can {
    protected Can successor;

    public abstract boolean store(Water water);

    public void storage(Water water) {
        if (store(water) == false && successor != null) {
            successor.storage(water);
        }
    }

    public void setSuccessor(Can can) {
        this.successor = can;
    }
}
