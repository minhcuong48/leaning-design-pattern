package com.cuong.learn_design_pattern.behavioral.mediator;

public abstract class User {
    public abstract String getName();

    public abstract void sendMessage(String message, User user);
    public abstract void getMessage(Message message);
}
