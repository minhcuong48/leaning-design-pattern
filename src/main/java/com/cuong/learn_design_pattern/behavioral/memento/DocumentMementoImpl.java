package com.cuong.learn_design_pattern.behavioral.memento;

public class DocumentMementoImpl extends DocumentMemento {
    private String text;

    public DocumentMementoImpl(String text) {
        this.text = text;
    }

    @Override
    public String getState() {
        return text;
    }

    @Override
    public void setState(String text) {
        this.text = text;
    }
}
