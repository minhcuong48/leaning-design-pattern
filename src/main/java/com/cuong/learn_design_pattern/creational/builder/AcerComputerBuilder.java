package com.cuong.learn_design_pattern.creational.builder;

public class AcerComputerBuilder extends ComputerBuilder {
    private Computer.Cpu cpu;
    private Computer.RamType ramType;

    private boolean hasLan;
    private boolean hasThunderbolt;
    private boolean hasReaderCard;

    public AcerComputerBuilder(Computer.Cpu cpu, Computer.RamType ramType) {
        this.cpu = cpu;
        this.ramType = ramType;
    }

    @Override
    public ComputerBuilder hasLan(boolean hasLan) {
        this.hasLan = hasLan;
        return this;
    }

    @Override
    public ComputerBuilder hasThunderbolt(boolean hasThunderbolt) {
        this.hasThunderbolt = hasThunderbolt;
        return this;
    }

    @Override
    public ComputerBuilder hasReaderCard(boolean hasReaderCard) {
        this.hasReaderCard = hasReaderCard;
        return this;
    }

    public Computer build() {
        return new Computer(cpu, ramType, hasLan, hasThunderbolt, hasReaderCard);
    }
}
