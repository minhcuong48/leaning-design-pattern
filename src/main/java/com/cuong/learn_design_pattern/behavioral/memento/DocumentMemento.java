package com.cuong.learn_design_pattern.behavioral.memento;

public abstract class DocumentMemento {
    public abstract String getState();
    public abstract void setState(String text);
}
