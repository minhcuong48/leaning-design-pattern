package com.cuong.learn_design_pattern.behavioral.chain_of_responsibility;

public class Can2Liters extends Can {
    private static final String CAN_TYPE = "Can 2 liters";

    @Override
    public boolean store(Water water) {
        if (water.getCapacity() <= 2) {
            System.out.println(CAN_TYPE + " stored " + water.getCapacity() + " litters water");
            return true;
        } else {
            System.out.println(CAN_TYPE + " cant storage " + water.getCapacity() + " liters");
            return false;
        }
    }
}
