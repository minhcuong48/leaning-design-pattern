package com.cuong.learn_design_pattern.behavioral.command;

public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public void report(Boss toBoss) {
        System.out.println(name + " is reporting to " + toBoss.getName());
    }
}
