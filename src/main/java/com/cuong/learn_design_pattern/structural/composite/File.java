package com.cuong.learn_design_pattern.structural.composite;

public class File extends FileComponent {

    public File(String filename) {
        this.filename = filename;
    }

    @Override
    public void showHierarchy() {
        printLevel();
        System.out.println("File: " + filename);
    }
}
