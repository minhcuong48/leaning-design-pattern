package com.cuong.learn_design_pattern.creational.prototype;

public class Client {
    public static void main(String[] args) {
        LaptopPrototype prototype = new AsusLaptop("K501LX");
        AsusLaptop laptop1 = (AsusLaptop) prototype.clone();
        prototype.setModel("Nitro 5");
        System.out.println(laptop1.getModel());

        AsusLaptop laptop2 = (AsusLaptop) prototype.clone();
        System.out.println(laptop2.getModel());
    }
}
