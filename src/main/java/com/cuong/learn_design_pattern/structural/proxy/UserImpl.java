package com.cuong.learn_design_pattern.structural.proxy;

public class UserImpl extends User {
    private String name;

    public UserImpl(String name) {
        this.name = name;
    }

    @Override
    public void showName() {
        System.out.println("My name is " + this.name);
    }
}
