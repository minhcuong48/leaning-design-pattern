package com.cuong.learn_design_pattern.structural.bridge.implementor;

public class CpuCoreI3 extends Cpu {
    @Override
    public void run() {
        System.out.println("Run CPU Core I3");
    }
}
