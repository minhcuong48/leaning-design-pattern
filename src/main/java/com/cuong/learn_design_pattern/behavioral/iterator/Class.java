package com.cuong.learn_design_pattern.behavioral.iterator;

import java.util.ArrayList;

public class Class extends StudentIterable {
    ArrayList<Student> students = new ArrayList<>();

    public void addStudent(Student student) {
        students.add(student);
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    @Override
    public StudentIterator getIterator() {
        return new StudentIteratorImpl(this);
    }
}
