package com.cuong.learn_design_pattern.creational.singleton;

public class Client {
    public static void main(String[] args) {
        System.out.println(LonelyService.lonelyInstance().getServiceData());
        LonelyService.lonelyInstance().setServiceData("New service data");
    }
}
