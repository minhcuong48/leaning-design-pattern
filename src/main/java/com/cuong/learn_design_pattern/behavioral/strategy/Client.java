package com.cuong.learn_design_pattern.behavioral.strategy;

public class Client {
    public static void main(String[] args) {
        Software software = new Software(new Algorithm1());
        software.run();

        software = new Software((new Algorithm2()));
        software.run();
    }
}
