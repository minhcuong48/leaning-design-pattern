package com.cuong.learn_design_pattern.behavioral.state;

public class Client {
    public static void main(String[] args) {
        Door closedDoor = new Door(new ClosedDoorState());
        closedDoor.knock();

        Door openingDoor = new Door(new OpeningDoorState());
        openingDoor.knock();
    }
}
