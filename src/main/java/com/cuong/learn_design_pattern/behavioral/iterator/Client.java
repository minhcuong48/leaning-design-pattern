package com.cuong.learn_design_pattern.behavioral.iterator;

public class Client {
    public static void main(String[] args) {
        Class clazz = new Class();
        clazz.addStudent(new Student("Student 1"));
        clazz.addStudent(new Student("Student 2"));
        clazz.addStudent(new Student("Student 3"));

        StudentIterator studentIterator = clazz.getIterator();
        Student student = null;
        while ((student = studentIterator.getNext()) != null) {
            System.out.println(student.getName());
        }
    }
}
