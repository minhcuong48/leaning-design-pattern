package com.cuong.learn_design_pattern.behavioral.memento;

public class Document {
    private String text;

    public Document(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DocumentMemento createDocumentMamento() {
        return new DocumentMementoImpl(text);
    }

    public void setDocumentMemento(DocumentMemento documentMemento) {
        this.text = documentMemento.getState();
    }

    public void showText() {
        System.out.println(text);
    }
}
