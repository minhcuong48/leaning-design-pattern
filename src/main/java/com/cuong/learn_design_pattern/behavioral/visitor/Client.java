package com.cuong.learn_design_pattern.behavioral.visitor;

import com.cuong.learn_design_pattern.behavioral.visitor.elements.Book;
import com.cuong.learn_design_pattern.behavioral.visitor.elements.Ruler;
import com.cuong.learn_design_pattern.behavioral.visitor.visitors.ShowVisitor;
import com.cuong.learn_design_pattern.behavioral.visitor.visitors.ShowVisitorImpl;

public class Client {
    public static void main(String[] args) {
        Book book = new Book("Design Pattern Book Gang of Four");
        Ruler ruler = new Ruler(20);

        ShowVisitor showVisitor = new ShowVisitorImpl();
        book.accept(showVisitor);
        ruler.accept(showVisitor);
    }
}
