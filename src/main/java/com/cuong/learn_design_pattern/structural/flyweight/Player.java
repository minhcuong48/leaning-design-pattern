package com.cuong.learn_design_pattern.structural.flyweight;

public abstract class Player {
    public abstract void doMission();
    public abstract void assignWeapon(Weapon weapon);
}
