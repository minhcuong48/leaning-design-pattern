package com.cuong.learn_design_pattern.behavioral.command;

public class Boss {
    private String name;

    public Boss(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void askEmployeeToReport(ReportCommand command) {
        command.report(this);
    }
}
