package com.cuong.learn_design_pattern.creational.factory_method;

public class WordApplication extends Application {
    @Override
    public Document createDocument() {
        return new WordDocument();
    }
}
