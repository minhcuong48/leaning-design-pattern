package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public abstract class Phone {

    public abstract String getDeviceName();

}
