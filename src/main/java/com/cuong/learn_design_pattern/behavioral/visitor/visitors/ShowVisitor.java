package com.cuong.learn_design_pattern.behavioral.visitor.visitors;

import com.cuong.learn_design_pattern.behavioral.visitor.elements.Book;
import com.cuong.learn_design_pattern.behavioral.visitor.elements.Ruler;

public abstract class ShowVisitor {
    public abstract void visit(Book book);
    public abstract void visit(Ruler ruler);
}
