package com.cuong.learn_design_pattern.behavioral.mediator;

public abstract class ChatSystemMediator {
    public abstract void addUser(User user);
    public abstract void sendMessage(Message message);
}
