package com.cuong.learn_design_pattern.creational.builder;

public abstract class ComputerBuilder {
    public abstract ComputerBuilder hasLan(boolean hasLan);
    public abstract ComputerBuilder hasThunderbolt(boolean hasThunderbolt);
    public abstract ComputerBuilder hasReaderCard(boolean hasReaderCard);
    public abstract Computer build();
}
