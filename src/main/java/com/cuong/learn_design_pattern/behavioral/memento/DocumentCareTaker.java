package com.cuong.learn_design_pattern.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

public class DocumentCareTaker {
    private List<DocumentMemento> documentMementos = new ArrayList<>();

    public void addDocumentMemento(DocumentMemento documentMemento) {
        documentMementos.add(documentMemento);
    }

    public DocumentMemento getLastDocumentMemento() {
        if(documentMementos.size() <= 0) {
            return null;
        }
        return documentMementos.get(documentMementos.size() - 1);
    }
}
