package com.cuong.learn_design_pattern.creational.prototype;

public class AsusLaptop extends LaptopPrototype {
    private String model;

    public AsusLaptop(String model) {
        this.model = model;
    }

    @Override
    public LaptopPrototype clone() {
        return new AsusLaptop(model);
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }
}
