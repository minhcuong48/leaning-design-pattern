package com.cuong.learn_design_pattern.creational.factory_method;

public abstract class Document {
    public abstract String getType();
}
