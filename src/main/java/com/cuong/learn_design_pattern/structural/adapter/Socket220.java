package com.cuong.learn_design_pattern.structural.adapter;

public class Socket220 {
    public Volts getVolts() {
        return new Volts(220);
    }
}
