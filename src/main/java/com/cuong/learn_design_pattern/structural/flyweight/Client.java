package com.cuong.learn_design_pattern.structural.flyweight;

public class Client {
    public static void main(String[] args) {
        Player p = PlayerFlyweightFactory.getPlayer(PlayerType.Terrorist);
        p.assignWeapon(Weapon.AK);

        p.doMission();

        p = PlayerFlyweightFactory.getPlayer(PlayerType.CounterTerrorist);
        p.assignWeapon(Weapon.M4A1);
        p.doMission();
    }
}
