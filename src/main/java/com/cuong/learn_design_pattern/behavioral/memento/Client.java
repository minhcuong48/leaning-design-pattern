package com.cuong.learn_design_pattern.behavioral.memento;

public class Client {
    public static void main(String[] args) {
        DocumentCareTaker documentCareTaker = new DocumentCareTaker();

        Document document = new Document("Version 1");
        document.showText();
        documentCareTaker.addDocumentMemento(document.createDocumentMamento());
        document.setText("Version 2");
        document.showText();
        document.setDocumentMemento(documentCareTaker.getLastDocumentMemento());
        document.showText();
    }
}
