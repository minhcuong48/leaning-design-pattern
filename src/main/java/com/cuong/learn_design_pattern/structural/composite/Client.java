package com.cuong.learn_design_pattern.structural.composite;

public class Client {
    public static void main(String[] args) {
        FileComponent folder1 = new Folder();
        FileComponent folder2 = new Folder();
        FileComponent file1 = new File("File 1");
        FileComponent file2 = new File("File 2");
        FileComponent file3 = new File("File 3");

        folder1.addComponent(folder2);
        folder1.addComponent(file1);
        folder2.addComponent(file2);
        folder2.addComponent(file3);

        folder1.showHierarchy();
    }
}
