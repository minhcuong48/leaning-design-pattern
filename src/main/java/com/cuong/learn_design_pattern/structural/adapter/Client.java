package com.cuong.learn_design_pattern.structural.adapter;

public class Client {
    public static void main(String[] args) {
        SocketAdapter10Volts socketAdapter10Volts = new SocketAdapter10VoltsImpl();
        SocketAdapter20Volts socketAdapter20Volts = new SocketAdapter20VoltsImpl();

        System.out.println(socketAdapter10Volts.get10Volts());
        System.out.println(socketAdapter20Volts.get20Volts());
    }
}
