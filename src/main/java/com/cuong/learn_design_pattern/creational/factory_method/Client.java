package com.cuong.learn_design_pattern.creational.factory_method;

public class Client {
    public static void main(String[] args) {
        WordApplication wordApplication = new WordApplication();
        wordApplication.createAndOpenDocument();

        ExcelApplication excelApplication = new ExcelApplication();
        excelApplication.createAndOpenDocument();
    }
}
