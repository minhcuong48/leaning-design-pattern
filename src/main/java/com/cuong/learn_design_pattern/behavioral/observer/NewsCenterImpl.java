package com.cuong.learn_design_pattern.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class NewsCenterImpl extends NewsCenter {
    private List<NewsReader> readers = new ArrayList<>();

    @Override
    public void addSubcriber(NewsReader reader) {
        readers.add(reader);
    }

    @Override
    public void postNews(String news) {
        for(NewsReader reader : readers) {
            reader.getNews(news);
        }
    }
}
