package com.cuong.learn_design_pattern.behavioral.chain_of_responsibility;

public class Can8Liters extends Can {
    private static final String CAN_TYPE = "Can 8 liters";

    @Override
    public boolean store(Water water) {
        if (water.getCapacity() <= 8) {
            System.out.println(CAN_TYPE + " stored " + water.getCapacity() + " litters water");
            return true;
        } else {
            System.out.println(CAN_TYPE + " cant storage " + water.getCapacity() + " liters");
            return false;
        }
    }
}
