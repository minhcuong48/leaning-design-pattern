package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public class Iphone extends Phone {
    @Override
    public String getDeviceName() {
        return "Iphone";
    }
}
