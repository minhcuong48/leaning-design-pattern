package com.cuong.learn_design_pattern.structural.composite;

import java.util.ArrayList;
import java.util.List;

public abstract class FileComponent {
    protected String filename;
    protected List<FileComponent> components = new ArrayList<>();
    protected int level;

    public FileComponent() {
        this.level = 0;
    }

    public void addComponent(FileComponent component) {
        this.components.add(component);
        component.level = this.level + 1;
    }

    public void removeComponent(int index) {
        this.components.get(index).level = 0;
        this.components.remove(index);
    }

    public FileComponent getComponent(int index) {
        return this.components.get(index);
    }

    public abstract void showHierarchy();

    protected void printLevel() {
        for(int i = 0; i < this.level; i++) {
            System.out.print("  ");
        }
    }
}
