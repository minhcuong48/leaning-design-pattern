package com.cuong.learn_design_pattern.behavioral.strategy;

public abstract class Algorithm {
    public abstract void runAlgorithm();
}
