package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public class GalaxyPhone extends Phone {
    @Override
    public String getDeviceName() {
        return "Samsung Galaxy Phone";
    }
}
