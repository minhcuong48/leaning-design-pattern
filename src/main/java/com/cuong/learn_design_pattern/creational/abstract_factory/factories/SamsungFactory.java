package com.cuong.learn_design_pattern.creational.abstract_factory.factories;

import com.cuong.learn_design_pattern.creational.abstract_factory.products.GalaxyPhone;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Laptop;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Phone;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.SamsungLaptop;

public class SamsungFactory extends DeviceFactory {
    @Override
    public Phone createPhone() {
        return new GalaxyPhone();
    }

    @Override
    public Laptop createLaptop() {
        return new SamsungLaptop();
    }
}
