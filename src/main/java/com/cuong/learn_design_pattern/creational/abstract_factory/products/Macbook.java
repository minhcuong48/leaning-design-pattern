package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public class Macbook extends Laptop {
    @Override
    public String getDeviceName() {
        return "Macbook";
    }
}
