package com.cuong.learn_design_pattern.structural.proxy;

public class UserProxy extends User {
    private User user;
    private String name;

    public UserProxy(String name) {
        this.name = name;
    }

    @Override
    public void showName() {
        if(user == null) {
            user = new UserImpl(name);
        }
        user.showName();
    }
}
