package com.cuong.learn_design_pattern.creational.builder;

public class Computer {
    public enum Cpu {
        CoreI3, CoreI5, CoreI7
    }

    public enum RamType {
        DDR3, DDR4, DDR5
    }

    private Cpu cpu;
    private RamType ramType;

    private boolean hasLan;
    private boolean hasThunderbolt;
    private boolean hasReaderCard;

    public Computer(Cpu cpu, RamType ramType, boolean hasLan, boolean hasThunderbolt, boolean hasReaderCard) {
        this.cpu = cpu;
        this.ramType = ramType;
        this.hasLan = hasLan;
        this.hasThunderbolt = hasThunderbolt;
        this.hasReaderCard = hasReaderCard;
    }

    @Override
    public String toString() {
        return cpu + "; "
                + ramType
                + "; hasLan: "
                + hasLan
                + "; hasThunderbolt: "
                + hasThunderbolt
                + "; hasReaderCard: "
                + hasReaderCard;
    }
}
