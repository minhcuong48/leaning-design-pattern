package com.cuong.learn_design_pattern.structural.decorator;

public abstract class Person {
    public abstract void live();
}
