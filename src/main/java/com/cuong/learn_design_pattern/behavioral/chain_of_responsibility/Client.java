package com.cuong.learn_design_pattern.behavioral.chain_of_responsibility;

public class Client {
    public static void main(String[] args) {
        Can can2liters = new Can2Liters();
        Can can4liters = new Can4Liters();
        Can can6liters = new Can6Liters();
        Can can8liters = new Can8Liters();

        can2liters.setSuccessor(can4liters);
        can4liters.setSuccessor(can6liters);
        can6liters.setSuccessor(can8liters);

        Water water1 = new Water(1);
        Water water3 = new Water(3);
        Water water5 = new Water(5);
        Water water7 = new Water(7);

        can2liters.storage(water1);
        can2liters.storage(water3);
        can2liters.storage(water5);
        can2liters.storage(water7);
    }
}
