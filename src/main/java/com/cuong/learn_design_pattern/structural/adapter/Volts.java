package com.cuong.learn_design_pattern.structural.adapter;

public class Volts {
    private int volts;

    public Volts(int volts) {
        this.volts = volts;
    }

    public int getVolts() {
        return volts;
    }

    @Override
    public String toString() {
        return String.valueOf(getVolts());
    }
}
