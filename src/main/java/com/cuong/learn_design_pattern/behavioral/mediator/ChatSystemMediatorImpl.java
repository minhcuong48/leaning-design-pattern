package com.cuong.learn_design_pattern.behavioral.mediator;

import java.util.ArrayList;

public class ChatSystemMediatorImpl extends ChatSystemMediator {
    private ArrayList<User> users = new ArrayList<>();

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void sendMessage(Message message) {
        message.getReceiver().getMessage(message);
    }
}
