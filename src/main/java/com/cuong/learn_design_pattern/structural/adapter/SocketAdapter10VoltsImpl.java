package com.cuong.learn_design_pattern.structural.adapter;

public class SocketAdapter10VoltsImpl extends SocketAdapter10Volts {
    Socket220 socket220 = new Socket220();

    @Override
    public Volts get10Volts() {
        return convertVolts(socket220.getVolts(), 22);
    }

    private Volts convertVolts(Volts volts, int x) {
        return new Volts(volts.getVolts()/x);
    }
}
