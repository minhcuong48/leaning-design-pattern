package com.cuong.learn_design_pattern.behavioral.state;

public class ClosedDoorState extends DoorState{
    @Override
    public void respond() {
        System.out.println("Door is closed");
    }
}
