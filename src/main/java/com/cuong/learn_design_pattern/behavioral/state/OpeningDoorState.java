package com.cuong.learn_design_pattern.behavioral.state;

public class OpeningDoorState extends DoorState {
    @Override
    public void respond() {
        System.out.println("Door is opening");
    }
}
