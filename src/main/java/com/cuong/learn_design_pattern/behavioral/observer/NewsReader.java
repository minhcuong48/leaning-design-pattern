package com.cuong.learn_design_pattern.behavioral.observer;

public abstract class NewsReader {
    public abstract void getNews(String news);
}
