package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public abstract class Laptop {

    public abstract String getDeviceName();

}
