package com.cuong.learn_design_pattern.behavioral.observer;

public abstract class NewsCenter {
    public abstract void addSubcriber(NewsReader reader);
    public abstract void postNews(String news);
}
