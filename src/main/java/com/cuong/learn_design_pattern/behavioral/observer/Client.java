package com.cuong.learn_design_pattern.behavioral.observer;

public class Client {
    public static void main(String[] args) {
        NewsReader reader1 = new NewsReaderImpl("Reader 1");
        NewsReader reader2 = new NewsReaderImpl("Reader 2");
        NewsReader reader3 = new NewsReaderImpl("Reader 3");

        NewsCenter center = new NewsCenterImpl();
        center.addSubcriber(reader1);
        center.addSubcriber(reader3);

        center.postNews("Today is really hot!");
    }
}
