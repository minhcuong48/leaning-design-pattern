package com.cuong.learn_design_pattern.structural.adapter;

public abstract class SocketAdapter10Volts {
    public abstract Volts get10Volts();
}
