package com.cuong.learn_design_pattern.creational.abstract_factory.factories;

import com.cuong.learn_design_pattern.creational.abstract_factory.products.Laptop;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Phone;

public abstract class DeviceFactory {

    public abstract Phone createPhone();

    public abstract Laptop createLaptop();

}
