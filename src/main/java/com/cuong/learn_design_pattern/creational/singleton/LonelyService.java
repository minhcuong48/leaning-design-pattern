package com.cuong.learn_design_pattern.creational.singleton;

public class LonelyService {
    private static LonelyService lonelyService;

    public static LonelyService lonelyInstance() {
        if(lonelyService != null) {
            return lonelyService;
        } else {
            return new LonelyService();
        }
    }

    private String serviceData;

    private LonelyService() {

    }

    public String getServiceData() {
        return serviceData;
    }

    public void setServiceData(String serviceData) {
        this.serviceData = serviceData;
        System.out.println("Set data to: " + this.serviceData);
    }
}
