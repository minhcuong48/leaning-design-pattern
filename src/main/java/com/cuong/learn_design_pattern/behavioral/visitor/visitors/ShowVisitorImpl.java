package com.cuong.learn_design_pattern.behavioral.visitor.visitors;

import com.cuong.learn_design_pattern.behavioral.visitor.elements.Book;
import com.cuong.learn_design_pattern.behavioral.visitor.elements.Ruler;

public class ShowVisitorImpl extends ShowVisitor {
    @Override
    public void visit(Book book) {
        System.out.println(book.getTitle());
    }

    @Override
    public void visit(Ruler ruler) {
        System.out.println(ruler.getLength());
    }
}
