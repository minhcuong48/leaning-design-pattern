package com.cuong.learn_design_pattern.structural.bridge.implementor;

public abstract class Cpu {
    public abstract void run();
}
