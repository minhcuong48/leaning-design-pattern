package com.cuong.learn_design_pattern.behavioral.command;

public abstract class ReportCommand {
    public abstract void report(Boss toBoss);
}
