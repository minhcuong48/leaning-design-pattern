package com.cuong.learn_design_pattern.structural.adapter;

public class SocketAdapter20VoltsImpl extends Socket220 implements SocketAdapter20Volts {

    @Override
    public Volts get20Volts() {
        return convertVolts(getVolts(), 11);
    }

    private Volts convertVolts(Volts volts, int x) {
        return new Volts(volts.getVolts()/x);
    }
}
