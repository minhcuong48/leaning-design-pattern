package com.cuong.learn_design_pattern.structural.bridge;


import com.cuong.learn_design_pattern.structural.bridge.abstraction.Computer;
import com.cuong.learn_design_pattern.structural.bridge.abstraction.Macbook;
import com.cuong.learn_design_pattern.structural.bridge.abstraction.SurfaceBook;

public class Client {
    public static void main(String[] args) {
        Computer macbook = new Macbook();
        Computer surfaceBook = new SurfaceBook();

        macbook.run();
        surfaceBook.run();
    }
}
