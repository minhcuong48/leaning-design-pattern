package com.cuong.learn_design_pattern.structural.decorator;

public class Client {
    public static void main(String[] args) {
        Person person = new PersonImpl();
        PersonDecorator woman = new Woman(person);
        woman.live();
    }
}
