package com.cuong.learn_design_pattern.creational.factory_method;

public abstract class Application {
    public abstract Document createDocument();

    public void createAndOpenDocument() {
        Document document = createDocument();
        System.out.println(document.getType());
    }
}
