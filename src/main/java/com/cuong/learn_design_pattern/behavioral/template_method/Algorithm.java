package com.cuong.learn_design_pattern.behavioral.template_method;

public abstract class Algorithm {
    public void runAlgorithm() {
        System.out.println("Before step 1");
        step1();
        System.out.println("Before step 2");
        step2();
        System.out.println("Step 3");
    }

    protected abstract void step1();
    protected abstract void step2();
}
