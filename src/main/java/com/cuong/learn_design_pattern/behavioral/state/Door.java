package com.cuong.learn_design_pattern.behavioral.state;

public class Door {
    private DoorState doorState;

    public Door(DoorState doorState) {
        this.doorState = doorState;
    }

    public void knock() {
        doorState.respond();
    }
}
