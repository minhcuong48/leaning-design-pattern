package com.cuong.learn_design_pattern.behavioral.visitor.elements;

import com.cuong.learn_design_pattern.behavioral.visitor.visitors.ShowVisitor;

public abstract class ShowVisitable {
    public abstract void accept(ShowVisitor showVisitor);
}
