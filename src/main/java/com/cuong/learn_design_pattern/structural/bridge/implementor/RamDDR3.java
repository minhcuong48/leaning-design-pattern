package com.cuong.learn_design_pattern.structural.bridge.implementor;

public class RamDDR3 extends Ram {
    @Override
    public void run() {
        System.out.println("Run RAM DDR3");
    }
}
