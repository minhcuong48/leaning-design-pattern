package com.cuong.learn_design_pattern.behavioral.template_method;

public class ConcreteAlgorithm extends Algorithm {
    @Override
    protected void step1() {
        System.out.println("Step 1");
    }

    @Override
    protected void step2() {
        System.out.println("Step 2");
    }
}
