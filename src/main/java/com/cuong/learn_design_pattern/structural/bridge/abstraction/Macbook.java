package com.cuong.learn_design_pattern.structural.bridge.abstraction;

import com.cuong.learn_design_pattern.structural.bridge.implementor.Cpu;
import com.cuong.learn_design_pattern.structural.bridge.implementor.CpuCoreI7;
import com.cuong.learn_design_pattern.structural.bridge.implementor.Ram;
import com.cuong.learn_design_pattern.structural.bridge.implementor.RamDDR4;

public class Macbook extends Computer {
    public Macbook() {
        this.cpu = new CpuCoreI7();
        this.ram = new RamDDR4();
    }

    @Override
    public void run() {
        System.out.println("Macbook");
        this.cpu.run();
        this.ram.run();
    }
}
