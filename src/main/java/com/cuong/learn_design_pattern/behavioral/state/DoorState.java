package com.cuong.learn_design_pattern.behavioral.state;

public abstract class DoorState {
    public abstract void respond();
}
