package com.cuong.learn_design_pattern.structural.decorator;

public class Woman extends PersonDecorator {
    public Woman(Person person) {
        super(person);
    }

    @Override
    public void live() {
        person.live();
        makingBaby();
    }

    private void makingBaby() {
        System.out.println("Making Baby");
    }
}
