package com.cuong.learn_design_pattern.creational.prototype;

public abstract class LaptopPrototype {
    public abstract LaptopPrototype clone();
    public abstract String getModel();
    public abstract void setModel(String model);
}
