package com.cuong.learn_design_pattern.behavioral.visitor.elements;

import com.cuong.learn_design_pattern.behavioral.visitor.visitors.ShowVisitor;

public class Book extends ShowVisitable {
    private String title;

    public Book(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void accept(ShowVisitor showVisitor) {
        showVisitor.visit(this);
    }
}
