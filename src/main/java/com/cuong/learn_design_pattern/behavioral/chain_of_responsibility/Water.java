package com.cuong.learn_design_pattern.behavioral.chain_of_responsibility;

public class Water {
    private int capacity;

    public Water(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }
}
