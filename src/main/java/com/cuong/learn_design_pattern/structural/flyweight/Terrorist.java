package com.cuong.learn_design_pattern.structural.flyweight;

public class Terrorist extends Player {
    private Misson misson;  // shared
    private Weapon weapon;  // non-shared

    public Terrorist(Misson misson) {
        this.misson = misson;
    }

    @Override
    public void doMission() {
        System.out.println("Mission: " + misson + "; Weapon: " + weapon);
    }

    @Override
    public void assignWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
