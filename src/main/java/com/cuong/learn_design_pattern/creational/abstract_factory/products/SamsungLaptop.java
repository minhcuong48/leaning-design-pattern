package com.cuong.learn_design_pattern.creational.abstract_factory.products;

public class SamsungLaptop extends Laptop {
    @Override
    public String getDeviceName() {
        return "Samsung Laptop";
    }
}
