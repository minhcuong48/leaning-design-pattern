package com.cuong.learn_design_pattern.structural.decorator;

public abstract class PersonDecorator extends Person {
    protected Person person;

    public PersonDecorator(Person person) {
        this.person = person;
    }

    public abstract void live();
}
