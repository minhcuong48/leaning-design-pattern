package com.cuong.learn_design_pattern.structural.bridge.abstraction;

import com.cuong.learn_design_pattern.structural.bridge.implementor.Cpu;
import com.cuong.learn_design_pattern.structural.bridge.implementor.Ram;

public abstract class Computer {
    protected Cpu cpu;
    protected Ram ram;

    public abstract void run();
}
