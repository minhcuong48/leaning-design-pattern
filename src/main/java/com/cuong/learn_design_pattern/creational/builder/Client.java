package com.cuong.learn_design_pattern.creational.builder;

public class Client {
    public static void main(String[] args) {
        Computer computer = new AcerComputerBuilder(Computer.Cpu.CoreI5, Computer.RamType.DDR4)
                .hasLan(true)
                .hasReaderCard(false)
                .build();

        System.out.println("Computer Info:");
        System.out.println(computer);
    }
}
