package com.cuong.learn_design_pattern.structural.flyweight;

public enum PlayerType {
    Terrorist, CounterTerrorist
}
