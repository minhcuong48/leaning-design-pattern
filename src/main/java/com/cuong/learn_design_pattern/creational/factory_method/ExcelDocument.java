package com.cuong.learn_design_pattern.creational.factory_method;

public class ExcelDocument extends Document {
    @Override
    public String getType() {
        return "Excel Document";
    }
}
