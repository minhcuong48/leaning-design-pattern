package com.cuong.learn_design_pattern.creational.abstract_factory.factories;

import com.cuong.learn_design_pattern.creational.abstract_factory.products.Iphone;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Laptop;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Macbook;
import com.cuong.learn_design_pattern.creational.abstract_factory.products.Phone;

public class AppleFactory extends DeviceFactory {
    @Override
    public Phone createPhone() {
        return new Iphone();
    }

    @Override
    public Laptop createLaptop() {
        return new Macbook();
    }
}
