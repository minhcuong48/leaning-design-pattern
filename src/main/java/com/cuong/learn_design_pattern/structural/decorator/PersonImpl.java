package com.cuong.learn_design_pattern.structural.decorator;

public class PersonImpl extends Person {
    @Override
    public void live() {
        System.out.println("Living");
    }
}
