package com.cuong.learn_design_pattern.behavioral.iterator;

public abstract class StudentIterator {
    public abstract Student getNext();
}
