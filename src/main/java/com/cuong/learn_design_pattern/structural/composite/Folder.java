package com.cuong.learn_design_pattern.structural.composite;

public class Folder extends FileComponent {
    @Override
    public void showHierarchy() {
        printLevel();
        System.out.println("Folder");
        for (FileComponent component : components) {
            component.showHierarchy();
        }
    }
}
