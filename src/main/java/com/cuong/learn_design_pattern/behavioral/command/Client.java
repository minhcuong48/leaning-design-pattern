package com.cuong.learn_design_pattern.behavioral.command;

public class Client {
    public static void main(String[] args) {
        Boss boss = new Boss("Boss A");
        Employee employee = new Employee("Employee B");

        ReportCommand reportCommand = new ReportCommandImpl(employee);
        boss.askEmployeeToReport(reportCommand);
    }
}
